@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Artegic Simulator</h1>
                <hr>

            </div>
            <div class="col-sm-12">
                <h2>Parameter bestimmen</h2>
                <form action="{{ route('simulation.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="production_time">Produktionsdauer pro Einheit in Sekunden</label>
                        <input type="number" class="form-control" id="production_time" name="production_time">
                    </div>

                    <div class="form-group">
                        <label for="route_length">Länge der Strecke</label>
                        <input type="number" class="form-control" id="route_length" name="route_length">
                    </div>

                    <div class="form-group">
                        <label for="transporter_capacity">Kapazität des Transportfahrzeugs in Einheiten</label>
                        <input type="number" class="form-control" id="transporter_capacity" name="transporter_capacity">
                    </div>

                    <div class="form-group">
                        <label for="transporter_loading_time">Ein- und Ausladegeschwindigkeit in Sekunden</label>
                        <input type="number" class="form-control" id="transporter_loading_time" name="transporter_loading_time">
                    </div>

                    <button type="submit" class="btn btn-primary">Simulation starten</button>

                </form>
            </div>
        </div>
    </div>

@endsection