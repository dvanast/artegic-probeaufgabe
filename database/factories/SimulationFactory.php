<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Simulation::class, function (Faker $faker) {
    return [
        'transporter_id' => factory(\App\Transporter::class)->create()->id,
        'warehouse_id' => factory(\App\Warehouse::class)->create()->id,
        'factory_id' => factory(\App\Factory::class)->create()->id,
        'route_length' => $faker->numberBetween(50, 500),
    ];
});
