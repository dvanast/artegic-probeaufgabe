<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Job::class, function (Faker $faker) {
    return [
        'production_time' => $faker->numberBetween(1, 100),
        'route_length' => $faker->numberBetween(100, 2500),
        'transporter_capacity' => $faker->numberBetween(5, 20),
        'transporter_loading_time' => $faker->numberBetween(1, 10),
    ];
});
