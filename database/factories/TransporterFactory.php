<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Transporter::class, function (Faker $faker) {
    return [
        'speed' => 10,
        'loading_time' => $faker->numberBetween(3, 20),
        'capacity' => $faker->numberBetween(10,20),
    ];
});
