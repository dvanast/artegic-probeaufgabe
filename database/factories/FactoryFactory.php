<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Factory::class, function (Faker $faker) {
    return [
        'production_time' => $faker->numberBetween(1, 10),
    ];
});
