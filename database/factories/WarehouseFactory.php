<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Warehouse::class, function (Faker $faker) {
    return [
        'stock' => 0,
    ];
});
