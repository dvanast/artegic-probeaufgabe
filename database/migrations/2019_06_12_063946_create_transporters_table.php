<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transporters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('capacity');
            $table->unsignedInteger('loading_time');
            $table->unsignedInteger('speed')->default(10);
            $table->dateTime('loading_at')->nullable();
            $table->boolean('is_loading')->default(false);
            $table->dateTime('driving_at')->nullable();
            $table->boolean('is_driving')->default(false);
            $table->unsignedDecimal('position')->default(0);
            $table->unsignedInteger('stock')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transporters');
    }
}
