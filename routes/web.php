<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('job', 'JobController@store')->name('job.store');

Route::get('simulation/{simulation}', 'SimulationController@show')->name('simulation.show');
Route::get('simulation/{simulation}/update', 'SimulationController@update')->name('simulation.update');
Route::get('simulation/{simulation}/reset', 'SimulationController@reset')->name('simulation.reset');
Route::post('simulation', 'SimulationController@store')->name('simulation.store');