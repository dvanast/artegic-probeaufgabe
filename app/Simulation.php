<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Transport\Transport;

class Simulation extends Model
{
    protected $fillable = [
        'transporter_id',
        'warehouse_id',
        'factory_id',
        'route_length'
    ];

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function factory()
    {
        return $this->belongsTo(Factory::class);
    }

    public function transporter()
    {
        return $this->belongsTo(Transporter::class);
    }
}
