<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transporter extends Model
{
    protected $fillable = [
        'capacity',
        'loading_time',
        'is_driving',
        'is_loading',
        'driving_at',
        'loading_at',
        'position'
    ];

    //protected $visible = ['position', 'capacity', 'loading_time'];

    public function transportGoods()
    {
        $this->is_driving = true;
        $this->driving_at = Carbon::now();
        $this->is_loading = false;
        $this->save();
    }

    public function emptyRide()
    {
        $this->is_driving = true;
        $this->driving_at = Carbon::now();
        $this->is_loading = false;
        $this->save();
    }

    public function loadGoods()
    {
        $this->simulation->factory->subStock($this->capacity);
        $this->is_loading = true;
        $this->is_driving = false;
        $this->loading_at = Carbon::now();
        $this->stock = $this->capacity;
        $this->save();
    }

    public function unloadGoods()
    {
        $this->warehouse->addStock($this->stock);

        $this->is_loading = true;
        $this->is_driving = false;
        $this->loading_at = Carbon::now();
        $this->stock = 0;
        $this->position = $this->simulation->route_length;
        $this->save();
    }

    public function getPositionAttribute()
    {
        if ($this->is_driving) {
            $time_since_start_driving = Carbon::parse($this->driving_at)->diffInSeconds(Carbon::now());
            if ($this->stock) {
                return $this->speed * $time_since_start_driving;
            }else {
                return $this->simulation->route_length - $this->speed * $time_since_start_driving;
            }
        } else
            return $this->getOriginal('position');
    }

    public function simulation()
    {
        return $this->hasOne(Simulation::class);
    }

    public function getDrivingTimeAttribute()
    {
        $this->simulation->route_length / $this->speed;
    }

    public function getWarehouseAttribute()
    {
        return $this->simulation->warehouse;
    }

    public function isLoadingReady()
    {
        return $this->is_loading && Carbon::parse($this->loading_at)->diffInSeconds(Carbon::now()) >= $this->loading_time;
    }

    public function isDrivingReady()
    {
        return $this->is_driving && Carbon::parse($this->driving_at)->diffInSeconds(Carbon::now()) >= $this->driving_time;
    }

    public function canUnloadGoods()
    {
        return $this->stock > 0 && $this->position >= $this->simulation->route_length;
    }

    public function canLoadGoods()
    {
        return $this->simulation->factory->stock >= $this->capacity && $this->position <= 0;
    }

    public function waitForGoods()
    {
        $this->is_driving = false;
        $this->position = 0;
        $this->save();
    }
}
