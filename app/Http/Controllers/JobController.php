<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{

    public function show(Job $job)
    {

    }

    public function store(Request $request)
    {
        $request->validate([
            'production_time' => 'required|numeric',
            'route_length' => 'required|numeric',
            'transporter_capacity' => 'required|numeric',
            'transporter_loading_time' => 'required|numeric',
        ]);

        Job::create($request->all());

        return redirect()->route('home');
    }
}
