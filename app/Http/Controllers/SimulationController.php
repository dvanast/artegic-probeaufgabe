<?php

namespace App\Http\Controllers;

use App\Factory;
use App\Simulation;
use App\Transporter;
use App\Warehouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Mail\Transport\Transport;

class SimulationController extends Controller
{
    public function show(Simulation $simulation)
    {
        return view('simulation.show')->with(compact('simulation'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'production_time' => 'required|numeric',
            'route_length' => 'required|numeric',
            'transporter_capacity' => 'required|numeric',
            'transporter_loading_time' => 'required|numeric',
        ]);

        $factory = Factory::create(['production_time' => $request->get('production_time')]);
        $transporter = Transporter::create(['capacity' => $request->get('transporter_capacity'), 'loading_time' => $request->get('transporter_loading_time')]);
        $warehouse = Warehouse::create();

        $simulation = Simulation::create([
            'transporter_id' => $transporter->id,
            'warehouse_id' => $warehouse->id,
            'factory_id' => $factory->id,
            'route_length' => $request->get('route_length'),
        ]);

        return redirect()->route('simulation.show', $simulation);
    }

    public function update(Simulation $simulation)
    {
        //Factory Produces
        $simulation->factory->updateStock();

        //Transporter
        if ($simulation->transporter->isLoadingReady()) {
            //Transporter has loaded Goods and is ready to drive to the warehouse
            $simulation->transporter->transportGoods();
        } elseif ($simulation->transporter->isDrivingReady()) {
            //When the Transporter arrives - unload if its at the warehouse otherwise wait for new goods at the factory
            if ($simulation->transporter->canUnloadGoods())
                $simulation->transporter->unloadGoods();
            elseif ($simulation->transporter->position <= 0) {
                $simulation->transporter->waitForGoods();
            }
        }elseif($simulation->transporter->canLoadGoods()) {
            //Load goods if there is enough on stock of the factory
            $simulation->transporter->loadGoods();
        }

        return response()->json($simulation->with('warehouse', 'transporter', 'factory')->where('id', $simulation->id)->first());
    }

    public function reset(Simulation $simulation)
    {
        $simulation->transporter->stock = 0;
        $simulation->transporter->is_driving = 0;
        $simulation->transporter->is_loading = 0;
        $simulation->transporter->position = 0;
        $simulation->transporter->save();

        $simulation->warehouse->stock = 0;
        $simulation->warehouse->save();

        $simulation->factory->stock = 0;
        $simulation->factory->save();

        return redirect()->route('simulation.show', $simulation);
    }
}
