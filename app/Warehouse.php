<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $fillable = ['stock'];

    public function addStock($amount = 1)
    {
        $this->stock = $this->stock + $amount;
        $this->save();
    }

    public function subStock($amount = 1)
    {
        $this->stock = $this->stock - $amount;
        $this->save();
    }
}
