<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
    protected $fillable = [ 'production_time', 'produced_at' ];

    public function addStock($amount=1)
    {
        $this->stock = $this->stock + $amount;
        $this->produced_at = Carbon::now();
        $this->save();

        return $this->stock;
    }

    public function subStock($amount = 1)
    {
        $this->stock = $this->stock - $amount;
        $this->save();

        return $this->stock;
    }

    public function updateStock()
    {
        $time_since_last_production = Carbon::parse($this->produced_at)->diffInSeconds(Carbon::now());
        if ($time_since_last_production >= $this->production_time)
            $this->addStock(1);
    }
}
