<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'production_time',
        'route_length',
        'transporter_capacity',
        'transporter_loading_time'
    ];

    public function getStockAttribute()
    {
        $start_time = Carbon::parse($this->created_at);
        $seconds_since_start = Carbon::now()->diffInSeconds($start_time);

        return floor($seconds_since_start / $this->production_time);
    }

    public function isTransporterRiding()
    {
        $start_time = Carbon::parse($this->created_at);
        $seconds_since_start = Carbon::now()->diffInSeconds($start_time);

        $seconds_since_last_ride =

        $transport_time = $this->loading_time*2 + $this->route_length / 10;


    }

    public function lastRide()
    {
        //Is Stock build up until transporter is back?
        $time_till_transporter_capacity_is_made = $this->production_time * $this->transporter_capacity;
        if ($time_till_transporter_capacity_is_made <= $this->rideTime())
            return Carbon::parse($this->created_at)->addSeconds($this->countTransporterRides() * $this->rideTime());
        else {
            $time_delta = $time_till_transporter_capacity_is_made - $this->rideTime();

            //TODO How many rides are made???



        }
    }

    public function rideTime()
    {
        $transport_time = $this->loading_time*2 + $this->route_length / 10;
        //$production_time_to_fill_transporter_capacity = $this->production_time * $this->transporter_capacity;

        return $transport_time;// + $production_time_to_fill_transporter_capacity;
    }

    public function countTransporterRides()
    {
        $start_time = Carbon::parse($this->created_at);
        $seconds_since_start = Carbon::now()->diffInSeconds($start_time);

        return floor($seconds_since_start / $this->rideTime());
    }

    public function getTransporterFreeCapacityAttribute()
    {
        $free_capacity = 0;

        if (!$this->isTransporterRiding()) {
            if ($this->stock <= $this->transporter_capacity)
                $free_capacity = $this->transporter_capacity - $this->stock;
        }

        return $free_capacity;
    }

}
