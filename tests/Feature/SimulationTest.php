<?php

namespace Tests\Feature;

use App\Factory;
use App\Simulation;
use App\Transporter;
use App\User;
use Carbon\Carbon;
use Illuminate\Mail\Transport\Transport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SimulationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_user_can_start_a_simulation()
    {
        $this->withoutExceptionHandling();
        $this->post(route('simulation.store'), [
            'production_time' => 10,
            'route_length' => 5000,
            'transporter_capacity' => 12,
            'transporter_loading_time' => 1
        ]);

        $this->assertDatabaseHas('factories', ['production_time' => 10]);
        $this->assertDatabaseHas('transporters', ['capacity' => 12, 'loading_time' => 1]);
        $this->assertDatabaseHas('warehouses', ['stock' => 0]);
        $this->assertDatabaseHas('simulations', ['route_length' => 5000]);

    }

    /** @test */
    public function it_delivers_data_for_the_simulation()
    {
        $simulation = factory(Simulation::class)->create();

        $response = $this->get(route('simulation.update', $simulation));

        $response->assertJson(['warehouse'=>['id' => $simulation->warehouse->id], 'transporter' => ['id' => $simulation->transporter->id], 'factory' => ['id' => $simulation->factory->id]] );
    }

    /** @test */
    public function it_will_update_produced_goods()
    {
        $factory = factory(Factory::class)->create(['production_time' => 1, 'produced_at' => Carbon::now()->subSecond()]);
        $simulation = factory(Simulation::class)->create([
            'factory_id' => $factory->id,
        ]);

        $response = $this->get(route('simulation.update', $simulation));

        $response->assertJson(['warehouse'=>['id' => $simulation->warehouse->id], 'transporter' => ['id' => $simulation->transporter->id], 'factory' => ['id' => $factory->id, 'stock' => 1]] );
    }

    /** @test */
    public function the_transporter_loads_goods()
    {
        $this->withoutExceptionHandling();
        $factory = factory(Factory::class)->create(['stock' => 10, 'produced_at' => Carbon::now()->subSecond()]);
        $transporter = factory(Transporter::class)->create(['capacity' => 10, 'position' => 0]);
        $simulation = factory(Simulation::class)->create([
            'factory_id' => $factory->id,
            'transporter_id' => $transporter->id
        ]);

        $response = $this->get(route('simulation.update', $simulation));

        $response->assertJson(['transporter' => ['id' => $transporter->refresh()->id, 'is_loading' => 1]] );

    }

    /** @test */
    public function the_transporter_drives_to_the_warehouse()
    {
        $this->withoutExceptionHandling();
        $transporter = factory(Transporter::class)->create(['capacity' => 10, 'stock' => 10, 'loading_time' => 1, 'loading_at' => Carbon::now()->subSecond(), 'is_loading' => true]);
        $simulation = factory(Simulation::class)->create([
            'transporter_id' => $transporter->id
        ]);

        $response = $this->get(route('simulation.update', $simulation));

        $response->assertJson(['transporter' => ['id' => $transporter->refresh()->id, 'is_loading' => 0, 'is_driving' => 1]] );

    }

    /** @test */
    public function the_transporter_unloads_goods()
    {
        $this->withoutExceptionHandling();
        $transporter = factory(Transporter::class)->create(['capacity' => 10, 'stock' => 10, 'loading_time' => 1, 'is_loading' => false, 'is_driving' => true, 'driving_at' => Carbon::now()->subSeconds(10), 'speed' => 10]);
        $simulation = factory(Simulation::class)->create([
            'transporter_id' => $transporter->id,
            'route_length' => 100
        ]);

        $response = $this->get(route('simulation.update', $simulation));

        $response->assertJson(['transporter' => ['id' => $transporter->refresh()->id, 'is_loading' => 1, 'is_driving' => 0, 'position' => 100]] );
    }

    /** @test */
    public function the_transporter_drives_back_to_factory_after_unloading_goods_at_warehouse()
    {
        $this->withoutExceptionHandling();
        $transporter = factory(Transporter::class)->create(['capacity' => 10, 'stock' => 0, 'loading_time' => 1, 'is_loading' => true, 'loading_at'=>Carbon::now()->subSecond(),'is_driving' => false, 'driving_at' => Carbon::now()->subSeconds(10), 'speed' => 10]);
        $simulation = factory(Simulation::class)->create([
            'transporter_id' => $transporter->id,
            'route_length' => 100
        ]);

        $response = $this->get(route('simulation.update', $simulation));

        $response->assertJson(['transporter' => ['id' => $transporter->refresh()->id, 'stock'=>0, 'is_loading' => 0, 'is_driving' => 1, 'position' => 100]] );
    }

    /** @test */
    public function the_transporter_updates_the_position()
    {
        $this->withoutExceptionHandling();
        $transporter = factory(Transporter::class)->create(['capacity' => 10, 'stock' => 0, 'loading_time' => 1, 'is_loading' => false, 'is_driving' => true, 'driving_at' => Carbon::now()->subSeconds(1), 'speed' => 10]);
        $simulation = factory(Simulation::class)->create([
            'transporter_id' => $transporter->id,
            'route_length' => 100
        ]);

        $response = $this->get(route('simulation.update', $simulation));

        $response->assertJson(['transporter' => ['id' => $transporter->refresh()->id, 'stock'=>0, 'is_loading' => 0, 'is_driving' => 1, 'position' => 90]] );
    }
}
